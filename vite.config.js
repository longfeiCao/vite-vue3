import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
export default defineConfig({
  // 项目插件
  plugins: [vue()],
  // 基础配置
  base: '/',
  publicDir: 'public',
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
  // 服务配置
  server: {
    host:'0.0.0.0',
    port: 9527,
    https: false,
    open: true,
    proxy: {
      // '/dev/api': {
      //   target: 'http://www.lwp.fun:7000',
      //   changeOrigin: true,
      //   rewrite: path => path.replace(/\/dev\/api/, ''),
      // },
      '/api/v1': {
        // target: 'http://192.168.1.17:9393',
        target: 'http://192.168.1.34:9393',
        changeOrigin: true,
        rewrite: path => path.replace(/\/api\/v1/, '/api/v1'),
      },
      '/my': {
        target: 'http://192.168.1.24:3000',
        changeOrigin: true,
        rewrite: path => path.replace(/\/my/, '/my'),
      },
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        modifyVars: {
          '@border-color-base': '#dce3e8',
        },
        javascriptEnabled: true,
      },
    },
  },
  build: {
    outDir: 'dist',
    assetsDir: 'assets',
    assetsInlineLimit: 4096,
    cssCodeSplit: true,
    sourcemap: false,
  },
})
