import { createFromIconfontCN } from '@ant-design/icons-vue'

const IconFont = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_2456157_4ovzopz659q.js',
  extraCommonProps: {
    style: {
      fontSize: '18px',
    },
  },
})

export default IconFont
