const exampleCenter = {
  path: 'exampleCenter',
  name: 'exampleCenter',
  meta: {
    title: '示例中心',
    icon: 'AppstoreTwoTone',
  },
  component: () => import('@/views/exampleCenter/index.vue'),
  redirect: '/exampleCenter/qrCode',
  children: [
    {
      path: 'qrCode',
      name: 'qrCode',
      meta: {
        title: '二维码生成',
        icon: 'InteractionTwoTone',
      },
      component: () => import('@/views/exampleCenter/qrCode.vue'),
    },
    {
      path: 'wangEditor',
      name: 'wangEditor',
      meta: {
        title: '富文本编辑',
        icon: 'InteractionTwoTone',
      },
      component: () => import('@/views/exampleCenter/wangEditor.vue'),
    },
    {
      path: 'baseForm',
      name: 'baseForm',
      meta: {
        title: '基础表单',
        icon: 'EditTwoTone',
      },
      component: () => import('@/views/exampleCenter/baseForm.vue'),
    },
    {
      path: 'baseTable',
      name: 'baseTable',
      meta: {
        title: '基础表格',
        icon: 'ProfileTwoTone',
      },
      component: () => import('@/views/exampleCenter/baseTable.vue'),
    },
  ],
}
export default exampleCenter
