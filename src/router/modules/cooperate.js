const cooperate = {
  path: 'cooperate',
  name: 'cooperate',
  meta: {
    title: '合作管理',
    icon: 'PieChartTwoTone',
  },
  component: () => import('@/views/cooperate/index.vue'),
  redirect:'/cooperate/liveroom/list',
  children:[{
    path:'liveroom',
    name:'cooperate-liveroom',
    meta:{
      title:'合作直播间',
      icon: 'PieChartTwoTone',
    },
    component: () => import('@/views/cooperate/liveroom/index.vue'),
    redirect:'/cooperate/liveroom/list',
    children:[
      {
        path:'list',
        name:'cooperate-liveroom-list',
        meta:{
          title:'列表',
          hide:true
        },
        component: () => import('@/views/cooperate/liveroom/list.vue'),
      },
      {
      path:'edit/:id?',
      name:'cooperate-liveroom-edit',
      meta:{
        title:'新增',
        hide:true
      },
      component: () => import('@/views/cooperate/liveroom/edit.vue'),
    }
  ]
  }]
}
export default cooperate
