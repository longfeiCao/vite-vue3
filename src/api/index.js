import * as role from './role'
import * as menu from './menu'
import * as login from './login'
import * as common from './common'
import * as cooperate from './cooperate'

let Api = {
    ...role,
    ...menu,
    ...login,
    ...common,
    ...cooperate
}

export default Api
