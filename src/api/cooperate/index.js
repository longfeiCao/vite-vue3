import request from '@/utils/request'

/** 用户登录 */
export function getLiveRoomList(data) {
  return request({
    url: '/api/v1/liveRoom/managementCooperativeLive',
    method: 'post',
    data,
  })
}
// L0001-绑定抖音账号
export function getBindDouyinNo(params) {
  return request({
    url: '/api/v1/liveRoom/bindDouyinNo',
    method: 'get',
    params,
  })
}
//确定合作
export function affirmBind(data) {
  return request({
    url: '/api/v1/liveRoom/affirmBind',
    method: 'post',
    data,
  })
}

//获取直播间详情
export function getLiveRoomInfo(params) {
  return request({
    url: '/api/v1/liveRoom/getLiveRoomInfo',
    method: 'get',
    params,
  })
}

// L0005解除直播间合作
export function relieveLiveRoom(params) {
  return request({
    url: '/api/v1/liveRoom/relieveLiveRoom',
    method: 'get',
    params,
  })
}

//重新合作
export function collaborateAgain(data) {
  return request({
    url: '/api/v1/liveRoom/collaborateAgain',
    method: 'post',
    data,
  })
}





